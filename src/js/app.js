var menuOpen = 0;
$(".dropdown").each(function(){
    $(this).click(function(){
        if (menuOpen === 0){
            $(this).find(".dropdown-menu").removeClass("hidden");
            menuOpen = 1;
        } else if (menuOpen === 1){
            $(this).find(".dropdown-menu").addClass("hidden");
            menuOpen = 0;
        }
    });
});

$(".modal-close, .modal:after").click(function() {
    $(".modal-container").hide().addClass("hidden");
    $(".modal").hide().addClass("hidden");
});

function addUsers() {
    $(".modal").show();
    $("#modal-add-users").show();
    //$("#modal-add-users").removeClass("hidden");
}
function addTicket() {
    $(".modal").show();
    $("#modal-add-ticket").show();
    //$("#modal-add-users").removeClass("hidden");
}

function editTicketType() {
    $(".modal").show();
    $("#modal-edit-ticket-type").show();
    //$("#modal-add-users").removeClass("hidden");
}
function createComplementary() {
    $(".modal").show();
    $("#modal-create-complementary").show();
    //$("#modal-add-users").removeClass("hidden");
}
